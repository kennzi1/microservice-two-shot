from django.db import models
from django.urls import reverse

class BinVO(models.Model):
  import_href = models.CharField(max_length=100, unique=True, null=True)
  bin_number = models.PositiveSmallIntegerField()

class Shoe(models.Model):
  """
  The Shoe model represents a shoe that will get loaded into the bin
  """

  manufacturer = models.CharField(max_length=50)
  model_name = models.CharField(max_length=50)
  color = models.CharField(max_length=20)
  picture_url = models.URLField(null=True)

  bin = models.ForeignKey(
    BinVO,
    related_name="shoes",
    on_delete=models.CASCADE,
  )

  def __str__(self):
      return self.model_name

def get_api_url(self):
    return reverse("api_shoe", kwargs={"pk": self.pk})
