from stat import FILE_ATTRIBUTE_DIRECTORY
from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return self.closet_name


class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    pircture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO, related_name="location", on_delete=models.CASCADE)