from django.urls import path, include
from .views import api_list_hats
from django.urls import path
from django.contrib import admin
// in the app urls:


urlpatterns = [
    path('hats/', api_list_hats, name="api_create_hats"),

]

// in the project urls:


urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include("hats_rest.urls")),
]
