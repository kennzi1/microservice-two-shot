import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='shoes'>
            <Route path='list' element={<ShoesList shoes={props.shoes} />} />
            <Route path='new' element={< ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


function App(props) {
  if (props.hats === undefined) {
    return null;
  }

  return (...

<Route path = "/hats" element = {< HatsList hats = { props.hats } />} />

);
}

// check links that exist in Nav.js to copy the same address