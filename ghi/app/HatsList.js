import React from 'react';

function HatsList(props) {
  return (
    <table>
      <thead>
      <tr>
        <th>Hat style</th>
        <th>Hat location</th>
      </tr>
      </thead>
      <tbody>
				{props.hats.map((hat, i) => {
          return (
            <tr key={i}>
            <td> { hat.style } </td>
            <td> { hat.location } </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default HatsList;