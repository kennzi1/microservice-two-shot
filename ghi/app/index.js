
async function loadHats() {
  const response = await fetch("http://localhost:8090/api/hats/");
  console.log(response);

if (response.ok) {

  let data = await response.json();
  console.log("DATA: ", data);
  root.render(
    <React.StrictMode>
      <App hats={data.hats}/>
    </React.StrictMode>
  );
}

else {
  console.error("response not ok", response);
}

}

loadHats() 